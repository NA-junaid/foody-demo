//add cart action
export const addAddon = (payload) => {
    return {
        type: 'ADD_ADDON',
        payload
    }
}

//remove from cart action
export const removeAddon = (payload) => {
    return {
        type: 'REOMVE_ADDON',
        payload
    }
}

// remove all items
export const discardAddons = () => {
    return {
        type: 'DISCARD_ADDONS'
    }
}