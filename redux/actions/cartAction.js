//add cart action
export const addToCart = (payload) => {
    return {
        type: 'ADD_TO_CART',
        payload
    }
}

//remove from cart action
export const removeFromCart = (payload) => {
    return {
        type: 'REOMVE_FROM_CART',
        payload
    }
}

//decrase quantity action
export const decreaseQuantity = (payload) => {
    return {
        type: 'DECREASE_QUANTITY',
        payload
    }
}
//increase quantity action
export const increaseQuantity = (payload) => {
    return {
        type: 'INCREASE_QUANTITY',
        payload
    }
}
// remove all items
export const discardCart = () => {
    return {
        type: 'DISCARD_CART'
    }
}