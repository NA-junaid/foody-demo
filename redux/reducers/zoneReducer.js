const zoneItem = (state, action) => {
    switch (action.type) {
        case 'ADD_ZONE':
            return action.payload;
        default:
            return state;
    }
}

export default function zoneReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_ZONE':
            let item = state.find(item =>  item.id == action.payload.id);
            if (item == undefined) {
                return [
                    ...state,
                    zoneItem(undefined, action)
                ];
            } else {
                return state;
            }
        default:
            return state;
    }
}