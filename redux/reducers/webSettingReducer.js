const initialState = {  };

export default function webSettingReducer(state = initialState, action) {
  switch (action.type) {
    case "SET_WEBSETTING":
      return action.payload;
    default:
      return state;
  }
}
