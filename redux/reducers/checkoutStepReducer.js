const initialState = 1;

export default function storeReducer(state = initialState, action) {
  switch (action.type) {
    case "SET_STEP":
      return action.payload;
    default:
      return state;
  }
}
