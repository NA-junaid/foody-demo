import React from "react";
function SubHeader(props) {
    return (
        <div className="sub_header">
            <h1 className="text-center">{props.title}</h1>
        </div>
    );
}
export default SubHeader;