import React from "react";
import Link from "next/link";
function RoundedButton(props){
  return(
    <Link href={props.link}>
    <button className="primary mt-3 mb-3" type="button">
       {props.title}
     </button>
     </Link>
  );
}
export default RoundedButton;
// export const RoundedButton =(props)=>{
//   return React.forwardRef(({ onClick, href }, ref) => {
//     return (
//       <a href={href} onClick={onClick} ref={ref} className="primary" type="button">
//       {props.title}
//     </a>
//     )
//   })
// }

