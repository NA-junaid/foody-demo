import React from "react";
import Link from "next/link";
import LanguageDropDown from "./LanguageDropDown";

function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function Header() {
  return (
    <div>
      <header className="header">
        <div className="d-flex justify-content-between">
          <span onClick={openNav}>
            <i>
              <img loading="lazy" src="/assets/img/menu_icon.svg" alt="menu" />
            </i>
          </span>
          <div className="cart_language">
            <span className="cart_bag">
              <Link href="/cart">
                <a title="cart">
                  <img loading="lazy" src="/assets/img/bag.svg" alt="cart" />
                </a>
              </Link>
            </span>
            <LanguageDropDown/>
          </div>
        </div>
      </header>
    </div>
  );
}

export default Header;
