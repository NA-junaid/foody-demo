import React, { useState, useEffect } from "react";
import SearchBox from "./SearchBox";
import CategoriesArea from "../partials/Home/CategoriesArea";
import Products from "../partials/Home/Products";
import CloseButton from "../partials/Home/CloseButton";

export default function SearchModel({ products }) {

  return (
    <div class="modal fade" id="openModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header ">
            <SearchBox />
           
            <button
                type="button"
                className="btn btn-dark"
                data-dismiss="modal"
                aria-label="Close"
            >
                <span aria-hidden="true">×</span>
            </button>
       
          </div>
          
          <div className="panel modal-body ">
            {products.map((product) => {
              return <div className="card-body" ><Products product={product} key={product.product_id} /></div>
            })}
         
          </div>
        </div>
      </div>
    </div>
  );


}



