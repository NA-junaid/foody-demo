import React from 'react';

function InnerHeaderBack(props) {
    return (
        <header className="inner_header">
            <div className="d-flex justify-content-between">
                <a href={props.link??"/"}>
                <i className="iconify" data-icon="ic:baseline-arrow-back" />
                </a>
            </div>
        </header>
    );
}
export default InnerHeaderBack;