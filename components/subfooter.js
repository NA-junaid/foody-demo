import React from "react";
import useTranslation from "../services/useTranslation";
function SubFooter() {
  const { all_rights } = useTranslation();
    return (
        <div className="sub_footer">
        <p className="text-mute p-3 m-0 text-center">{all_rights}</p>
      </div>
    );
}

export default SubFooter;