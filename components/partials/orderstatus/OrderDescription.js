import React from "react";
import useTranslation from "../../../services/useTranslation";

function OrderDescription() {
    const { order_lookup_description } = new useTranslation();
    return (
        <p>{order_lookup_description}</p>
    );
}

export default OrderDescription;
