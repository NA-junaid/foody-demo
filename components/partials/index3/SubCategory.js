import React from "react";

function SubCategory({categoryName,length}) {
    return (
        <div
            className="card-header collapsed mb-2 "
            data-toggle="collapse"
            href="#sac1"
        >
            <a className="card-title">
                {categoryName}<span>({length})</span>
            </a>
        </div>
    );
}

export default SubCategory;
