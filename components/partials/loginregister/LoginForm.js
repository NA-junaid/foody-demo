import React from "react";
import FormGroup from "../../common/FormGroup";
import RememberMe from "./RememberMe";
import ForgotPassword from "./ForgotPassword";
import RoundedButton from "../../common/RoundedButton";
import SubTitles from "../../common/SubTitles";
import useTranslation from "../../../services/useTranslation";
import networkService from "../../../services/networkService";
import urlService from "../../../services/urlService";
import { Formik } from "formik";
import axios from "axios";
import qs from "qs";
import Router from "next/router";


function LoginForm() {
  const {
    customer_login,
    email_address,
    password,
    login,
    name,
  } = useTranslation();
  const invalid_email = "Invalid Email Prvovided";
  const required = "Required";

  return (
    <div
      className="tab-pane fade show active"
      id="home"
      role="tabpanel"
      aria-labelledby="home-tab"
    >
      <Formik
        initialValues={{ email: "", password: "" }}
        validate={(values) => {
          const errors = {};
          if (!values.email) {
            errors.email = required;
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
          ) {
            errors.email = invalid_email;
          }
          if (!values.password) {
            errors.password = required;
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting }) => {
          setTimeout(() => {
            let data = {
              client_id: "11",
              client_secret: "E3WbxaE14ub0GG2GwWSp1NtPpTwUQYcPotcOC1p0",
              username: values.email,
              password: values.password,
              grant_type: "password",
            };

            const uri = "https://services.nextaxe.com/oauth/token?store_id=200";
            //const uri = 'http://localhost:3000/oauth/token?store_id=210';

            let options = {
              method: "POST",
              url: uri,
              data: qs.stringify(data),
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                //"Access-Control-Allow-Origin": "*",
              },
            };

            axios(options)
              .then((response) => {
                localStorage.setItem("token",response.data.access_token)
                Router.push('/');
              })
              .catch(function (error) {
                console.log(error);
              });

            setSubmitting(false);
          }, 400);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          /* and other goodies */
        }) => (
          <form onSubmit={handleSubmit}>
            <SubTitles title={customer_login} />

            <FormGroup
              label={email_address}
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              hint={email_address}
            />
            <p className="text-danger">
              {errors.email && touched.email && errors.email}
            </p>

            <FormGroup
              label={password}
              name="password"
              type="password"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
              hint="**********"
            />
            <p className="text-danger">
              {errors.password && touched.password && errors.password}
            </p>

            <div className="d-flex justify-content-between forgotpass pt-3 pb-3">
              <RememberMe />
              <ForgotPassword />
            </div>
            <button
              className="primary mt-3 mb-3"
              type="submit"
              disabled={isSubmitting}
            >
              {login}
            </button>
            {/* Sbumit */}
            {/* </button> */}
          </form>
        )}
      </Formik>
    </div>
  );
}

export default LoginForm;
