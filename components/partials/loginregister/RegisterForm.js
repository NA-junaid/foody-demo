import React from "react";
import SubTitles from "../../common/SubTitles";
import FormGroup from "../../common/FormGroup";
import RoundedButton from "../../common/RoundedButton";
import useTranslation from "../../../services/useTranslation";
import { Formik } from 'formik';
import urlService from "../../../services/urlService";
import networkService from "../../../services/networkService";

function RegisterForm() {
    const { customer_register, full_name, email_address, mobile, password, invalid_email, submit, confirm_password, register,name, required } = useTranslation();

    const onSubmitApi = async (values) => {
        const response = await networkService.post(urlService.postRegister,values);
        if (response.IsValid) {
          orders = response.Payload;
        }
        console.log(response);
      }

    return (
        <div
            className="tab-pane fade"
            id="profile"
            role="tabpanel"
            aria-labelledby="profile-tab"
        >
            <Formik
                initialValues={{ name: '', email: '', mobile: '', password: '', password_confirmation: '' }}
                validate={values => {
                    const errors = {};
                    if (!values.email) {
                        errors.email = required;
                    } else if (
                        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                    ) {
                        errors.email = invalid_email;
                    }
                    if (!values.name) {
                        errors.name = required;
                    }
                    if (!values.mobile) {
                        errors.mobile = required;
                    }
                    if (!values.password) {
                        errors.password = required;
                    }
                    if (values.password != values.password_confirmation) {
                        errors.password_confirmation = 'Confirm password not matched';
                    }
                    return errors;
                }}
                onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                        onSubmitApi(values);
                        setSubmitting(false);
                    }, 400);
                }}
            >

                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    /* and other goodies */
                }) => (
                        <form className="regsiter" onSubmit={handleSubmit}>
                            <SubTitles title={customer_register} />

                            <FormGroup label={name} name="name" onChange={handleChange} onBlur={handleBlur} value={values.name} hint={name} />
                            <p className="text-danger">{errors.name && touched.name && errors.name}</p>

                            <FormGroup label={email_address} name="email" onChange={handleChange} onBlur={handleBlur} value={values.email} hint={email_address} />
                            <p className="text-danger">{errors.email && touched.email && errors.email}</p>

                            <FormGroup label={mobile} name="mobile" onChange={handleChange} onBlur={handleBlur} value={values.mobile} hint={mobile} />
                            <p className="text-danger">{errors.mobile && touched.mobile && errors.mobile}</p>

                            <FormGroup label={password} name="password" type="password" onChange={handleChange} onBlur={handleBlur} value={values.password} hint="**********" />
                            <p className="text-danger">{errors.password && touched.password && errors.password}</p>

                            <FormGroup label={confirm_password} name="password_confirmation" type="password" onChange={handleChange} onBlur={handleBlur} value={values.password_confirmation} hint="**********" />
                            <p className="text-danger">{errors.password_confirmation && touched.password_confirmation && errors.password_confirmation}</p>

                            <button className="primary mt-3 mb-3" type="submit" disabled={isSubmitting}>
                                {submit}
                            </button>

                        </form>
                    )}
            </Formik>


        </div>
    );
}

export default RegisterForm;
