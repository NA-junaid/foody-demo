import React from "react";
import AddressList from "./AddressList";
import RoundedButton from "../../common/RoundedButton";
import Link from "next/link";
import useTranslation from "../../../services/useTranslation";
function WhiteWrapper({data}) {
    const { add_address } = useTranslation();
    return (
        <div className="white_wrapper cart">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <ul className="ul-account-list ul-address-book-list  pt-3">
                            {data.map(address => {
                                return <AddressList data={address} />
                            })}
                           
                        </ul>
                        <RoundedButton title={add_address} link="/addaddress" />
                    </div>
                </div>
            </div>
        </div>
    );
}
export default WhiteWrapper;