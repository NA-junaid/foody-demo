import FormGroup from "../../common/FormGroup";
import RoundedButton from "../../common/RoundedButton";
import useTranslation from "../../../services/useTranslation";
import { Formik } from 'formik';

function WhiteWrapper() {
    const { area_name, block, street_number, city, country, add_address } = useTranslation();
    return (
        <div className="white_wrapper cart">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <br />
                        <Formik
                            initialValues={{ area_name: '', block: '', street_number: '', city: '', country: '' }}
                            validate={values => {
                                const errors = {};
                                if (!values.area_name) {
                                    errors.area_name = 'Area Name is required';
                                }
                                if (!values.block) {
                                    errors.block = 'Block is required';
                                }
                                if (!values.street_number) {
                                    errors.street_number = 'Street Number is required';
                                }
                                if (!values.city) {
                                    errors.city = 'City is required';
                                }
                                if (!values.country) {
                                    errors.country = 'Country is required';
                                }
                                return errors;
                            }}
                            onSubmit={(values, { setSubmitting }) => {
                                setTimeout(() => {
                                    alert(JSON.stringify(values, null, 2));
                                    setSubmitting(false);
                                }, 400);
                            }}
                        >
                            {({
                                values,
                                errors,
                                touched,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                isSubmitting,
                                /* and other goodies */
                            }) => (
                                    <form onSubmit={handleSubmit}>
                                        <FormGroup label={area_name} name="area_name" onChange={handleChange} onBlur={handleBlur} value={values.area_name} hint={area_name} />
                                        <p className="text-danger">{errors.area_name && touched.area_name && errors.area_name}</p>

                                        <FormGroup label={block} name="block" onChange={handleChange} onBlur={handleBlur} value={values.block} hint={block} />
                                        <p className="text-danger">{errors.block && touched.block && errors.block}</p>

                                        <FormGroup label={street_number} name="street_number" onChange={handleChange} onBlur={handleBlur} value={values.street_number} hint={street_number} />
                                        <p className="text-danger">{errors.street_number && touched.street_number && errors.street_number}</p>

                                        <FormGroup label={city} name="city" onChange={handleChange} onBlur={handleBlur} value={values.city} hint={city} />
                                        <p className="text-danger">{errors.city && touched.city && errors.city}</p>

                                        <FormGroup label={country} name="country" onChange={handleChange} onBlur={handleBlur} value={values.country} hint={country} />
                                        <p className="text-danger">{errors.country && touched.country && errors.country}</p>

                                        <button className="primary mt-3 mb-3" type="submit" disabled={isSubmitting}>
                                            {add_address}
                                        </button>
                                    </form>
                                )}
                        </Formik>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default WhiteWrapper;