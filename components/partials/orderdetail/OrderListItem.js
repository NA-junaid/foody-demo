import React from "react";

function OrderListItem(props) {
    return (
        <li>
            <span className="d-block">
                {" "}
                <small>{props.title} </small>
            </span>
            <strong>{props.detail}</strong>{" "}
        </li>
    );
}

export default OrderListItem;