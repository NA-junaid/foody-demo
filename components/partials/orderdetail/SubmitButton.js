import useTranslation from "../../../services/useTranslation";
function SubmitButton() {
    const { re_order } = useTranslation();
    return (
        <button className="primary  mb-3">{re_order}</button>
    );
}
export default SubmitButton;