import React from "react";
import useTranslation from "../../../services/useTranslation";

function OrderStatus({ order_no, status, time }) {
    const { order_id } = useTranslation();
    const iconClass = order.status == 'Complete' ? 'check-circle-filled' : order.status == 'Failed' ? 'round-cancel' : 'check-circle-filled2';
    return (
        <div>
            <li>
                <strong>{order_id} : {order_no}</strong>{" "}
                <span className="delivered-btn pull-right">
                    <span className="iconify" data-icon={`ant-design:${iconClass}`} />
                    {status}
                </span>{" "}
                <span className="d-block">
                    {" "}
                    <small>{time}</small>
                </span>{" "}
            </li>
        </div>
    );
}

export default OrderStatus;