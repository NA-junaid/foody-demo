import useTranslation from "../../../services/useTranslation";
import { useSelector } from "react-redux";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";


function ShippingInfo() {
  const shippingMethods = useSelector((state) => state.shippingMethods);
  const [holder, setHolder] = useCheckoutDataHolder();

  const setShippingMethod = (shippingMethod) => {
    holder.shippingMethod = shippingMethod;
    setHolder(holder);
  }

  const { shipping_method } = useTranslation();
  return (
    <div>
      <h5>{shipping_method}</h5>
      <p></p>
      <ul className="payment_method">
        {shippingMethods.map((method) => {
          let id = `shipping-${method.id}`;
          let idHash = `#shipping-${method.id}`;
          return (
            <li key={id}>
              <div className="custom-control custom-radio">
                <input
                  type="radio"
                  id={id}
                  name="shippingMethod"
                  className="custom-control-input"
                  defaultChecked
                  onClick={() => setShippingMethod(method)}
                />
                <label className="custom-control-label" htmlFor={idHash}>
                  {method.name}
                </label>
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
export default ShippingInfo;
