import useTranslation from "../../../services/useTranslation";

import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { setPickup } from "../../../redux/actions/pickUpAction";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import Select from 'react-select';
// import Geocode from "react-geocode";

function SearchAddress() {
    const areas = useSelector((state) => state.areas);
    const [selected, setSelected] = useState(0);
    const dispatch = useDispatch();

    const onChanged = (val) => {
      setSelected(val);
      dispatch(setPickup({
        pickup: false,
        outlet: '',
        area: val
      }));
    }

    const handleLocation = () => {
        // console.log('asasdsa');
        getlocation();  
    }
   
    const handleLocationByInput = (val) => {
      getAddressByInput(val);
    };
    const handlePlacesSuggestions = () => {
       setTimeout(() => {
        googlearea();
      }, 400);
    };
  
    const [holder, setHolder] = useCheckoutDataHolder();
    // console.log('selected,areas,',holder);
    const { area_name } = useTranslation();

 const renderInput = () => {
         if(areas.length > 0 ){
            return (<select className="custom-seelct" 
                         style={{width: '82%',height: '55px'}} 
                         onChange={(event) => handleLocationByInput(event.target.value)}
                         >
                          <option>zz</option>
                          {areas.map((area) => (
                              <option 
                               >{area.name}</option>
                          ))}
                        </select>);
         }else{
            return (<input type="text" placeholder={area_name} name 
              onChange={() => handlePlacesSuggestions()}  
              id="delivery-area" />);
         }
  }
 
    return (
        <div className="search-address mb-4">
            <span className="location_search">
                <a href="#" onClick={()=> handleLocation()}>
                    <span
                        className="iconify"
                        data-inline="false"
                        data-icon="bx:bx-current-location"
                        style={{ color: "#ffffff", fontSize: 35 }}
                    />
                </a>
            </span>            
           {renderInput()}
        </div>
    );
}
export default SearchAddress;