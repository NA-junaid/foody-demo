import React, { useState, useEffect } from "react";
import PaymentMethod from "./PaymentMethod";
import CheckoutInfo from "./CheckoutInfo";
import ShippingInfo from "./ShipingInfo";
import SubmitButton from "./SubmitButton";
import RoundedButton from "../../common/RoundedButton";
import useTranslation from "../../../services/useTranslation";
import { Formik, Field, Form } from "formik";
import RadioButton from "./RadioButton";
import { useSelector } from "react-redux";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import Router from "next/router";

function StepThree() {
  const {
    payment_method,
    We_updates,
    Review_Order,
    shipping_method,
    required,
  } = useTranslation();
  
  const paymentMethods = useSelector((state) => state.paymentMethods);
  const shippingMethods = useSelector((state) => state.shippingMethods);
  const [holder, setHolder] = useCheckoutDataHolder();
  const [selectedShippingMethod, setSelectedShippingMethod] = useState();
  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState();
  useEffect(() => {
    if (holder.shippingMethod != null) {
      setSelectedShippingMethod(holder.shippingMethod.name);
    }

    if (holder.paymentMethod != null) {
      setSelectedPaymentMethod(holder.paymentMethod.slug);
    }
// console.log('paymentMethods,shippingMethods',paymentMethods,shippingMethods);
  });

  const renderShippingMethod = (errors, touched) => {
    if (!holder.pickup) {
      return (
        <div>
          <h5>{shipping_method}</h5>
          <p></p>
          <ul className="payment_method">
            {shippingMethods.map((method) => {
              return (
                <li key={method.name}>
                  <div className="custom-control custom-radio">
                    <label className="">
                      <Field
                        type="radio"
                        name="shippingMethod"
                        value={method.name}
                      />
                      {method.name} ({method.rate})
                    </label>
                  </div>
                </li>
              );
            })}
          </ul>
          <p className="text-danger">
            {errors.shippingMethod &&
              touched.shippingMethod &&
              errors.shippingMethod}
          </p>
        </div>
      );
    }
  };

  const renderPaymentMethod = (errors, touched) => {
    return (
      <div>
        <CheckoutInfo title={payment_method} subTitle={We_updates} />
        <ul className="payment_method">
          {paymentMethods.map((method) => {
            return (
              <li key={method.slug}>
                <div className="custom-control custom-radio">
                  <label className="">
                    <Field
                      type="radio"
                      name="paymentMethod"
                      //className="custom-control-input"
                      value={method.slug}
                    />
                    {method.name}
                  </label>
                </div>
              </li>
            );
          })}
        </ul>
        <p className="text-danger">
          {errors.paymentMethod &&
            touched.paymentMethod &&
            errors.paymentMethod}
        </p>
      </div>
    );
  };

  return (
    <div className="row setup-content" id="step-3">
      <div className="col-xs-6 ">
        <div className="col-md-12">
          <Formik
            initialValues={{
              paymentMethod: selectedShippingMethod,
              shippingMethod: selectedPaymentMethod,
            }}
            validate={(values) => {
              const errors = {};
              if (!values.paymentMethod) {
                errors.paymentMethod = required;
              }

              if (!holder.pickup && !values.shippingMethod) {
                errors.shippingMethod = required;
              }

              return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
              setTimeout(() => {
                setSubmitting(false);

                let paymentMethod = paymentMethods.filter(
                  (i) => i.slug == values.paymentMethod
                );
                let shippingMethod = shippingMethods.filter(
                  (i) => i.name == values.shippingMethod
                );

                holder.paymentMethod = paymentMethod[0];

                if (!holder.pickup) {
                  holder.shippingMethod = shippingMethod[0];
                }

                setHolder(holder);

                Router.push("/revieworder");
              }, 400);
              // onSubmitApi(values)
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
            }) => (
              <Form>
                {renderPaymentMethod(errors, touched)}
                {renderShippingMethod(errors, touched)}
                <button
                  className="btn btn-primary nextBtn btn-lg pull-right"
                  type="submit"
                  disabled={isSubmitting}
                >
                  {Review_Order}
                </button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
}
export default StepThree;
