function WhiteWrapper() {
    return (
        <div className="white_wrapper cart">
            <div className="container">
                <div className="row">
                    <div className="col-md-12 mt-3">
                        <div className="map mb-3 ">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1781287.9077480363!2d46.414478775959395!3d29.30938918651801!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3fc5363fbeea51a1%3A0x74726bcd92d8edd2!2sKuwait!5e0!3m2!1sen!2sin!4v1594107740960!5m2!1sen!2sin"
                                width="100%"
                                height={550}
                                frameBorder={0}
                                style={{ border: 0 }}
                                allowFullScreen
                                aria-hidden="false"
                                tabIndex={0}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        );
}
export default WhiteWrapper;