import React from "react";
import useTranslation from "../../../services/useTranslation";
import { useSelector } from "react-redux";

function ListItem({ order }) {
    const { order_id } = useTranslation();
    const store = useSelector((state) => state.store);
    const iconClass = order.status == 'Complete' ? 'check-circle-filled' : order.status == 'Failed' ? 'round-cancel' : 'check-circle-filled2';
    return (
        <li>
            <span className="order-id">{order_id} : {order.order_number}</span>
            <strong className="d-block pt-1 pb-1">{store.currency} : {order.total}</strong>
            <span className="timendate pull-right">
                {order.order_time}
            </span>
            <span className="qqty">x{order[order_items].length}</span>

        </li>
    );
}

export default ListItem;