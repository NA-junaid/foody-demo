import React from "react";
import ListItem from "./ListItem";
import Link from "next/link";


function WhiteWrapper({ orders }) {
  return (
    <div className="white_wrapper cart">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <ul className="ul-account-list ul-order-list pt-3">
              {orders.map(order => {
                return <Link Link href={{ pathname: '/orderdetail', query: { id: order.id } }}>
                  <ListItem key={val.id} order={order} /></Link>
              })}
            </ul>
          </div>
        </div>
      </div>
    </div>

  );
}

export default WhiteWrapper;