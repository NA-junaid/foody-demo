import React from "react";
import Link from 'next/link';


function ListItem(props) {
    return (
        <li>
            <Link href={props.link}>
                <a href="">
                    <span className="iconify" data-icon={props.dataicon} />
                 {" " + props.title} <i className="icon-arrow-right float-right" />
                </a>
            </Link>
        </li>
    );
}

export default ListItem;