import TopCategory from "./TopCategory";
import ProductGridItem from "./ProductGridItem";
import { useSelector } from "react-redux";
import useLanguage from "../../../services/useLanguage";

function Home2Products({ products }) {
  const categories = useSelector((state) => state.categories);
  const lang = useLanguage();
  return (
    <div className="home2_products_area">
      {categories.map(category => {
        let filteredProducts = products.filter(prod => {
          return prod.category_id == category.id;
        });
        if (filteredProducts != null && filteredProducts.length > 0) {
        return <div className="products_row">
          <TopCategory categoryName={category[lang]} products={filteredProducts} />
          <div className="product_scroller">
            <ul className="inner_pro">

              {
                filteredProducts.slice(0, 4).map(product => {
                  return (
                    <ProductGridItem data={product} />
                  )
                })
              }

            </ul>
            <hr />
          </div>
        </div>
        }
      })}
    </div>
  );
}
export default Home2Products;