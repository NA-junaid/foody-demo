import React from "react";
import useTranslation from "../../../services/useTranslation";

function SearchArea() {
  const { search_products } = useTranslation();

  return (
    <div className="col-md-12 mt-3">
      <input
        type="text"
        placeholder={search_products}
        className="form-control"
        data-toggle="modal" data-target="#openModal"
      />
    </div>
  );
}

export default SearchArea;
