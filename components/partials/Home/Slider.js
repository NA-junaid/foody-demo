import React from "react";
import Next from "./Next";
import Previous from "./Previous";
import { useSelector } from "react-redux";

function Slider({banners}) {
    const languages = useSelector((state) => state.languages);
    const filtered = languages.filter((item) => item.isDefault);
    const lang = filtered[0];

    return (
        <div>
            <div className="slider">
                <div
                    id="carouselExampleControls"
                    className="carousel slide"
                    data-ride="carousel"
                >
                    <div className="carousel-inner">
                        {
                            banners.map(banner => {
                                return (
                                    <div className="carousel-item active" key={banner}>
                                        <img
                                            loading="lazy"
                                            className="d-block w-100 banner"
                                            src={banner.url}
                                            alt="food"
                                            title={banner[lang.short_name]}
                                        />
                                    </div>
                                )
                            })
                        }
                    </div>
                    <Previous />
                    <Next />
                </div>
            </div>
        </div>
    );
}

export default Slider;
