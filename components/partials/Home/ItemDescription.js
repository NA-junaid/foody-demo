import React from "react";
import useCurrency from "../../../services/useCurrency";

function ItemDescription({ name, price }) {
    const currency = useCurrency();
    return (
        <div className="item_d">
            <p>{name}</p>
            <p>{`${currency} ${price}`}</p>
        </div>
    );
}

export default ItemDescription;
