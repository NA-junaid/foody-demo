import React from "react";
import { useSelector } from "react-redux";

function VendorDescription() {
    const store = useSelector((state) => state.store);
    return (
        <div className="col-md-8">
            <div className="v_detail">
                <p>
                    {store.disclaimer??''}
          </p>
            </div>
        </div>
    );
}

export default VendorDescription;
