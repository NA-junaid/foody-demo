import React from "react";
import useCurrency from "../../../services/useCurrency";
import { useDispatch, useSelector } from "react-redux";
import { addAddon, removeAddon } from "../../../redux/actions/addonAction";
import useLanguage from "../../../services/useLanguage";

function SelectAddOn({ addon, counter }) {
  const currency = useCurrency();
  const dispatch = useDispatch();
  const store = useSelector((state) => state.store);
  const product = useSelector((state) => state.popup);
  const lang = useLanguage();
  const addons = useSelector((state) => state.addons);

  let id = `collapse21-${counter}`;
  let idHashed = `#collapse21-${counter}`;
  let addonGroup = `group${counter}[]`;

  const toggleItem = (event, item, grounName, group) => {
    item.group = group;
    item.grounName = grounName;
    if (event.target.checked) {
      if (!group) {
        let addon = product[lang].addons.find((a) => a.name == grounName);
        let as = addons.filter((a) => a.grounName == grounName);

        if (addon.max == as.length) {
          event.target.checked = false;
          return;
        }
      }

      dispatch(addAddon(item));
    } else {
      dispatch(removeAddon(item));
    }
  };

  return (
    <div className="cat1 panel">
      <div
        className="card-header collapsed mb-2"
        data-toggle="collapse"
        href={idHashed}
      >
        <a className="card-title">{addon.name}</a>
      </div>
      <div
        id={id}
        className="card-body p-0 collapse "
        data-parent="#accordion2"
      >
        {addon.items.map((item, index) => {
          let selector = `select01-${item.name}-${addon.name}`;
          return (
            <div className="d-flex p-3 justify-content-between" key={item.name}>
              <div className="custom-control custom-radio">
                <input
                  type={addon.type == "add_on" ? "checkbox" : "radio"}
                  id={selector}
                  name={addonGroup}
                  className="custom-control-input"
                  onChange={(event) =>
                    toggleItem(
                      event,
                      item,
                      addon.name,
                      !(addon.type == "add_on")
                    )
                  }
                />
                <label className="custom-control-label" htmlFor={selector}>
                  {item.name}
                </label>
              </div>
              <label htmlFor={selector}>
                {currency} {parseFloat(item.price).toFixed(store.round_off)}
              </label>
            </div>
          );
        })}

        <hr />
      </div>
    </div>
  );
}

export default SelectAddOn;
