import React from "react";

function WhatsappIcon({ number }) {
  let link = `https://api.whatsapp.com/send?phone=${number}`;
  return (
    <div className="col-md-4">
      <a href={link} target="_blank">
        <span className="iconify" data-icon="uil:whatsapp" />
      </a>
    </div>
  );
}

export default WhatsappIcon;
