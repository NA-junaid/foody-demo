import React from "react";
import ImageBlock from "./ImageBlock";
import ItemDescription from "./ItemDescription";
import ShowMore from "./ShowMore";
import useLanguage from "../../../services/useLanguage";

function ProductGridItem({data}) {
    const lang = useLanguage();
    return (
        
        <li>
            <a href="#/">
                <ImageBlock img={data.image}/>
                <ItemDescription name={data[lang].name} price={data.retail_price}/>
                <ShowMore product={data}/>
            </a>
        </li>
    );
}

export default ProductGridItem;
