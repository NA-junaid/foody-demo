import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { setPickup } from "../../../redux/actions/pickUpAction";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import useTranslation from "../../../services/useTranslation";

function PickUpAddressList({ setLocation,setPickupTrue }) {
  const outlets = useSelector((state) => state.outlets);
  const [selected, setSelected] = useState(0);
  const pickUp = useSelector((state) => state.pickUp);
  const dispatch = useDispatch();
  const { pick_from } = useTranslation();
  const [holder, setHolder] = useCheckoutDataHolder();

  const onChanged = (val) => {


    setSelected(val);

    holder.pickup = true;
    holder.outlet = val;
    holder.area = null;

    setHolder(holder);
    setPickupTrue(true)
    setLocation(pick_from + " " + val.name+', '+val.street_1, val.city, val.country);

    $('#pickup_opup').modal('hide')
    //console.log(holder);
    //$(".selected_area").show();
  };

  return (
    outlets.length > 0 &&
    <div
      className="tab-pane fade"
      id="pills-profile"
      role="tabpanel"
      aria-labelledby="pills-profile-tab"
    >
      {/* Pickup address list*/}
      <ul className="pickup_list">
        {outlets.map((outlet) => {
          if (outlet.pickup == 1) {
            let id = `outlet-${outlet.id}`;
            let idHashed = `#outlet-${outlet.id}`;

            return (
              <li key={outlet.id}>
                <h3>
                  <label htmlFor={id}>{outlet.name}</label>
                </h3>
                <span>
                  <small>
                    <label htmlFor={id}>{outlet.street_1}, {outlet.city}, {outlet.country}</label>
                  </small>
                </span>
                <span className="select_a">
                  <div className="custom-control custom-radio">
                    <input
                      type="radio"
                      id={id}
                      name="customRadio"
                      value={outlet}
                      checked={outlet == selected}
                      onChange={() => onChanged(outlet)}
                      className="custom-control-input"
                      data-dismiss="modal"
                    />
                    <label className="custom-control-label" htmlFor={id} />
                  </div>
                </span>
              </li>
            );
          }
        })}
      </ul>
    </div>
  );
}

export default PickUpAddressList;
