import React from "react";
import { useSelector } from "react-redux";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import useTranslation from "../../../services/useTranslation";

function Change({pickupTrue}) {
  const { change } = new useTranslation();
  
  const deliveryPopUp = () => {
    jQuery("#pills-home").addClass("active show");
    jQuery("#pills-home-tab").addClass("active");

    jQuery("#pills-profile-tab").removeClass("active");
  };
  const pickupPopUP = () => {
    jQuery("#pills-profile").addClass("active show");
    jQuery("#pills-profile-tab").addClass("active show");

    jQuery("#pills-home-tab").removeClass("active show");
  };
  return (
    <div>
      <span>
        <a
          href="#"
          data-toggle="modal"
          data-target="#pickup_opup"
          className="change-add"
          onClick={pickupTrue ? pickupPopUP : deliveryPopUp}
        >
          {change}
        </a>
      </span>
    </div>
  );
}

export default Change;
