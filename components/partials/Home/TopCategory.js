import React from "react";
import Link from "next/link";
import useLanguage from "../../../services/useLanguage";

function TopCategory({ categoryName, products }) {
    const lang = useLanguage();
    const showmore ="Show more";
    return (
        <div className="d-flex mt-3 ml-3 mr-3  font-bold justify-content-between">
            <h6>{categoryName}</h6>
            <span>
                <Link href={{ pathname: '/moreitems', query: { data: JSON.stringify(products), categoryName: categoryName } }}>
                    <a href="#" className="show-more">
                        {showmore}
</a>
                </Link>
            </span>
        </div>
    );
}

export default TopCategory;
