import React, { useState } from "react";

import ProductGridItem from "../Home/ProductGridItem";
import TopCategory from "../Home/TopCategory";
import { useSelector } from "react-redux";
import useLanguage from "../../../services/useLanguage";

function ProductSection({ products }) {
    const categories = useSelector((state) => state.categories);
    const lang = useLanguage();
    const [subCat, setSubCat] = useState(0);
    
    return (
        <div>
            {
                categories.map(category => {
                    let filteredProducts = products.filter(prod => {
                        return prod.category_id == category.id;
                    });
                    if (filteredProducts != null && filteredProducts.length > 0) {
                        return (
                            <div>
                                <TopCategory categoryName={category[lang]} products={filteredProducts} />

                                <ul className="scrollspyArea">
                                    {
                                        category['children'].length > 0
                                            ? category['children'].map(sub => {
                                                return (
                                                    <li className={sub.id == subCat ? "active" : ""}>
                                                        <a href="#" onClick={() => onClick(sub.id)}>{sub[lang]}</a>
                                                    </li>
                                                );
                                            })
                                            : null
                                    }
                                </ul>

                                <div className="product_scroller">
                                    <ul className="inner_pro">

                                        {
                                            filteredProducts.slice(0, 4).map(product => {
                                                return (
                                                    <ProductGridItem data={product} />
                                                )
                                            })
                                        }

                                    </ul>
                                    <hr />
                                </div>
                            </div>
                        )
                    }
                })
            }

        </div>
    );
}

export default ProductSection;
