import React from "react";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import useTranslation from "../../../services/useTranslation";
function DateTime(){
const { time } = useTranslation();
const [holder, setHolder] = useCheckoutDataHolder();
    return(
        <p className=" pt-3 pb-3 m-0">
          <span>
            {" "}
            <b>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                aria-hidden="true"
                focusable="false"
                width="1em"
                height="1em"
                preserveAspectRatio="xMidYMid meet"
                viewBox="0 0 32 32"
                className="iconify"
                data-icon="carbon:time"
                style={{
                  verticalAlign: "-0.125em",
                  transform: "rotate(360deg)"
                }}
              >
                <path
                  d="M16 30a14 14 0 1 1 14-14a14 14 0 0 1-14 14zm0-26a12 12 0 1 0 12 12A12 12 0 0 0 16 4z"
                  fill="currentColor"
                />
                <path
                  d="M20.59 22L15 16.41V7h2v8.58l5 5.01L20.59 22z"
                  fill="currentColor"
                />
              </svg>
            </b>{" "}
                {time}: { holder.shippingMethod != null ? holder.shippingMethod.delivery_time : ''} 
              {/*on {new Date(new Date().setMinutes(new Date().getMinutes() - (holder.shippingMethod.delivery_time))).toISOString()}*/}
          </span>
        </p>
    );
}
export default DateTime;