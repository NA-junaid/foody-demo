import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import useTranslation from "../../../services/useTranslation";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import useCurrency from "../../../services/useCurrency";

function TotalSummary() {
  const { sub_total, Delivery, total,discount } = useTranslation();
  const cart = useSelector((state) => state.cart);
  const store = useSelector((state) => state.store);
  const [holder, setHolder] = useCheckoutDataHolder();
  const currency = useCurrency();
  const [cartTotal, setCartTotal] = useState(cart.total);
  let [cop_discount, setDisCop] = useState();

  let is_discount = false;
  holder.cop_discount = (9.234234).toFixed(store.round_off);

  if(holder.cop_discount > 0){
      is_discount = true;
  }else{
      holder.cop_discount = 0;
  }

  useEffect(() => {
    setDisCop(holder.cop_discount );
  console.log(holder);
    let t = cartTotal;
    if (holder.shippingMethod != null) {
      let t = parseFloat(cart.total) + parseFloat(holder.shippingMethod.rate) - parseFloat(holder.cop_discount);
      setCartTotal(t.toFixed(store.round_off));
    }
  });

  const renderShippingTitle = () => {
    if (holder.shippingMethod != null) {
      return (
        <>
          <p>{Delivery}</p>
          {/*<p>{"Time"}</p>*/}
          {holder.cop_discount > 0 ? <p>{discount}</p> : '' }
        </>
      );
    }
  };

  const renderShipping = () => {
    if (holder.shippingMethod != null) {
      return (
        <>
          <p>
            {currency} {holder.shippingMethod.rate.toFixed(store.round_off)}
          </p>
          {/*<p>{holder.shippingMethod.delivery_time}</p>*/}
           {holder.cop_discount > 0 ? <p> {currency} - {holder.cop_discount}</p> : '' }
        </>
      );
    }
  };

  const handleClick = () => {
      
  }

  return (
    <div>
      <div className="d-flex p-4 grey mt-2 mb-5 justify-content-between cart_total">
        <span>
          <p>{sub_total}</p>
          {renderShippingTitle()}
          <p>
            <b>{total}</b>
          </p>
        </span>
        <span className="text-right">
          <p>
            {currency} {cart.total}
          </p>
          {renderShipping()}
          <p>
            <b>
              {currency} {cartTotal}
            </b>
          </p>
        </span>
      </div>
    </div>
  );
}

export default TotalSummary;
