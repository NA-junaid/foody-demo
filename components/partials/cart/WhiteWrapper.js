import React from "react";
import CartListItem from "./CartListItems";
import DateTime from "./DateTime";
import PromotionCode from "./PromotionCode";
import TotalSummary from "./TotalSummary";
import useTranslation from "../../../services/useTranslation";
import { useSelector } from "react-redux";
import Link from "next/link";

function WhiteWrapper() {
  const { Order_Items, empty_cart, checkout } = useTranslation();
  const cart = useSelector((state) => state.cart);
  

  const renderCart = () => {
    if (cart.items.length > 0) {
      return (
        <div className="col-md-12">
          <h2 className="text-uppercase pt-3 pb-3">{Order_Items}</h2>
          <hr />
          <CartListItem />
          <hr />
          <DateTime />
          
          <hr />
          <PromotionCode />
          <TotalSummary />
          
          <Link href="/checkout">
          <a href="#" className="btn primary nextBtn btn-lg pull-right">
            {checkout}
          </a>
        </Link>


        </div>
      );
    } else {
      return (
        <div className="col-md-12">
          <img loading="lazy" src="/assets/img/bag.svg" alt="cart" />
          <h2 className="text-uppercase pt-3 pb-3">
            <Link href="/">
              <a href="#">
                <br />
                {empty_cart}
              </a>
            </Link>
          </h2>
        </div>
      );
    }
  };
  return (
    <div className="white_wrapper cart">
      <div className="container">
        <div className="row">{renderCart()}</div>
      </div>
    </div>
  );
}
export default WhiteWrapper;
