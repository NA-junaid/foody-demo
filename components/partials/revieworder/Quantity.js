import React from "react";
import useCurrency from "../../../services/useCurrency";

function Quantity({qty,ttl}) {
    const currency = useCurrency();
    return (
        <div>
            <span className="">x{qty}</span> <strong>{currency} : {ttl}</strong>{" "}
        </div>
    );
}

export default Quantity;
