import React from "react";

function CartItemName({name}) {
    return (
        <div>
            <span className="item_title">{name}</span>{" "}
        </div>
    );
}

export default CartItemName;
