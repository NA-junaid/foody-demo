
function swapStyleSheet(sheet) {
    document.getElementById("pagestyle").setAttribute("href", sheet);
}

$(function () {
    $(document).on("shown.bs.collapse", ".collapse", function (e) {
        var $panel = $(this).closest(".panel");
        $("html,body").animate(
            {
                scrollTop: $panel.offset().top,
            },
            500
        );
    });
});
function googlearea() {
    var input1 = '';
    var input = document.getElementById('delivery-area');
    autocomplete = new google.maps.places.Autocomplete(input);
    searchBox = new google.maps.places.SearchBox(input);
    setTimeout(() => {
        input1 = $('#delivery-area').val();
        console.log(input1);
        getAddressByInput(input1.split(", ", 1))
    }, 2000);
}
function getAddressByInput(keyword) {
    console.log('keyword', keyword);
    $.ajax({
        url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + keyword + '&sensor=false&key=AIzaSyDRCd_mF3VtrWp8rdRtnOjZkdE9P_-kxRc',
        success: function (data) {
            console.log(data);
            let country;
            let province;
            let city;
            let area;
            let block;
            let street;
            let street_number;
            let plot;

            $.each(data.results, function (index, value) {
                $.each(value.address_components, function (index, value) {
                    // console.log(value.types);
                    if (value.types[0] == 'country') {
                        country = value.long_name;
                    } else if (value.types[0] == 'administrative_area_level_1') {
                        province = value.long_name;
                    } else if (value.types[0] == 'locality' && value.types[1] == 'political') {
                        city = value.long_name;
                    } else if (value.types[0] == 'political' && value.types[1] == 'sublocality') {
                        area = value.long_name;
                    } else if (value.types[0] == 'neighborhood' && value.types[1] == 'political') {
                        block = value.long_name;
                    } else if (value.types[0] == 'route') {
                        street = value.long_name;
                    } else if (value.types[0] == 'street_number') {
                        street_number = value.long_name;
                    } else if (value.types[0] == 'premise') {
                        plot = value.long_name;
                    }
                });
            });

            // let res  = {
            //     'country' : country, 
            //     'city' : city, 
            //     'area' : keyword, 
            //     'block' : block, 
            //     'street_number' : street_number, 
            //     'street' : street, 
            //     'plot' : plot
            //   }
            // console.log(res);
            $('#edit_country').val(country);
            $('#edit_city').val(city);
            $('#edit_area').val(keyword).trigger('change');
            $('#block').val(block).trigger('change');
            $('#street').val(street_number).trigger('change');
            $('#house_detail').val(plot).trigger('change');
        }
    });
}
     
toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-full-width",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

function getAddressByLatLng(lat, long) {
    $.ajax({
        url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + long + '&sensor=false&key=AIzaSyDRCd_mF3VtrWp8rdRtnOjZkdE9P_-kxRc',
        success: function (data) {
            // console.log('aasdas',data.results[0].formatted_address);
            // $('#delivery-area').val(data.results[0].formatted_address);
            let country;
            let province;
            let city;
            let area;
            let block;
            let street;
            let street_number;
            let plot;

            $.each(data.results, function (index, value) {
                $.each(value.address_components, function (index, value) {
                    // console.log(value);
                    // console.log(value.types);
                    if (value.types[0] == 'country') {
                        country = value.long_name;
                    } else if (value.types[0] == 'administrative_area_level_1') {
                        province = value.long_name;
                    } else if (value.types[0] == 'locality' && value.types[1] == 'political') {
                        city = value.long_name;
                    } else if (value.types[0] == 'political' && value.types[1] == 'sublocality') {
                        area = value.long_name;
                    } else if (value.types[0] == 'neighborhood' && value.types[1] == 'political') {
                        block = value.long_name;
                    } else if (value.types[0] == 'route') {
                        street = value.long_name;
                    } else if (value.types[0] == 'street_number') {
                        street_number = value.long_name;
                    } else if (value.types[0] == 'premise') {
                        plot = value.long_name;
                    }
                });
            });
            // let res  = {
            //       'country' : country, 
            //       'city' : city, 
            //       'area' : area, 
            //       'block' : block, 
            //       'street_number' : street_number, 
            //       'street' : street, 
            //       'plot' : plot
            //     }
            // console.log(res);
            $('#edit_country').val(country);
            $('#edit_city').val(city);
            $('#edit_area').val(area).trigger('change');
            $('#block').val(block).trigger('change');
            $('#street').val(street_number ? street_number : street).trigger('change');
            $('#house_detail').val(plot).trigger('change');
        }
    });
}
function getLocation() {
    let map_message = 'Your location is not in selected area. Select location from map.';
    let latitude = '';
    let longitude = '';
    navigator.geolocation.getCurrentPosition(function (position) {
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;
    });
    // console.log(latitude);
    // console.log(longitude);
    // getAddressByLatLng(latitude, longitude);
    var myCenter = new google.maps.LatLng(latitude, longitude);
    var mapOptions = { center: myCenter, zoom: 17 };

    var mapCanvas = document.getElementById("map");
    var map = new google.maps.Map(mapCanvas, mapOptions);
    // marker.setMap(map);
    var data = JSON.parse($('#bonds').val() ? $('#bonds').val() : '');
    var center1 = JSON.parse($('#center').val() ? $('#center').val() : '');

    var strictBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(data.results[0].geometry.bounds.southwest),
        new google.maps.LatLng(data.results[0].geometry.bounds.northeast)
    );
    // Listen for the dragend event

    if (strictBounds.contains(myCenter) == true) { return; }
    let abc = window.confirm(map_message);
    console.log(abc, strictBounds.contains(myCenter));
    if (abc == true) {
        map.setCenter(new google.maps.LatLng(center1.lat, center1.lng));
        // return;
    } else {
        window.location.reload();

    }
    google.maps.event.addListener(map, 'drag', function () {
        if (strictBounds.contains(map.getCenter())) return;

        // We're out of bounds - Move the map back within the bounds

        var c = map.getCenter(),
            x = c.lng(),
            y = c.lat(),
            maxX = strictBounds.getNorthEast().lng(),
            maxY = strictBounds.getNorthEast().lat(),
            minX = strictBounds.getSouthWest().lng(),
            minY = strictBounds.getSouthWest().lat();

        if (x < minX) x = minX;
        if (x > maxX) x = maxX;
        if (y < minY) y = minY;
        if (y > maxY) y = maxY;

        map.setCenter(new google.maps.LatLng(y, x));
    });
    google.maps.event.addListener(map, 'zoom_changed', function () {
        if (map.getZoom() < 17) map.setZoom(17);
    });


    var marker = new google.maps.Marker({ position: myCenter });
    getLocationOnChange(map, marker)

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}
function getlocation() {
    //check if location enable or not
    navigator.geolocation.watchPosition(function (position) {
    },
        function (error) {
            if (error.code == error.PERMISSION_DENIED)
                alert("Please enable your location");;
        });


    // to get lat lng from current locations
    let latitude = 0;
    let longitude = 0;
    navigator.geolocation.getCurrentPosition(function (position) {
        console.log('position', position);
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;
        getAddressByLatLng(latitude, longitude);
    });
    console.log(longitude);
}