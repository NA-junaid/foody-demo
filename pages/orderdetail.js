import SubHeader from "../components/common/SubHeader";
import WhiteWrapper from "../components/partials/orderdetail/WhiteWrapper";
import useTranslation from "../services/useTranslation";
import { useRouter } from "next/router";

export default function OrderDetail() {
  const { order_detail } = useTranslation();
  const router = useRouter();
  const { id } = router.query;
  return (
    <div>
      
      <SubHeader title={order_detail} />
      <WhiteWrapper id={id}/>
    </div>
  );
}