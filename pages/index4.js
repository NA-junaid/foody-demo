import React from "react";
import Header from "../components/common/Header";
import Slider from "../components/partials/Home/Slider";
import VendorDetail from "../components/partials/Home/VendorDetail";
import SearchFields from "../components/partials/Home/SearchFields";
import PopUp from "../components/partials/Home/PopUp";
import FixedButtonArea from "../components/FixedButtonArea";
import DetailPopUp from "../components/partials/Home/DetailPopUp";
import ProductSection from "../components/partials/index4/ProductSection";


function Home() {
  return (
    <div>
      <Header />
      <Slider />
      <VendorDetail />
      <hr />
      <SearchFields />
      <div className="home2_products_area">
        <div className="products_row">
          <ProductSection />
          <PopUp />
          <DetailPopUp />
          <FixedButtonArea />
          <hr />
        </div>
      </div>
    </div>

  );
}

export default Home;
