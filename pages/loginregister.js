import SubHeader from "../components/common/SubHeader";
import NavItems from "../components/partials/loginregister/NavItems";
import LoginForm from "../components/partials/loginregister/LoginForm";
import RegisterForm from "../components/partials/loginregister/RegisterForm";
import InnerHeader from "../components/common/InnerHeader";
import useTranslation from "../services/useTranslation";

export default function LoginRegister() {
  const {login_and_register} = useTranslation();
  return (
    <div>
  
      <SubHeader title= {login_and_register} />

      <div className="white_wrapper cart">
        <div className="container">
          <div className="row">
            <div className="col-md-12 mt-3">
              <div className="login_register">
                <NavItems />
                <div className="tab-content" id="myTabContent">
                  <LoginForm />
                  <RegisterForm />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  );
}