import React from "react";
import Header from "../components/common/Header";
import Slider from "../components/partials/Home/Slider";
import VendorDetail from "../components/partials/Home/VendorDetail";
import SearchFields from "../components/partials/Home/SearchFields";
import PopUp from "../components/partials/Home/PopUp";
import DetailPopUp from "../components/partials/Home/DetailPopUp";
import FixedButtonArea from "../components/FixedButtonArea";
import MenuProduct from "../components/partials/index5/MenuProduct";
import useTranslation from "../services/useTranslation";

function Home() {
  const { Menu } = useTranslation();
  return (
    <div>
      <Header />
      <Slider />
      <VendorDetail />
      <SearchFields />
      {/* Products Categories */}
      <div className="home3_products_area">
        <h3 className="p-3">{Menu}</h3>
        <ul className="inner_pro">

          {
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(product => {
              return (
                <MenuProduct />
              )
            })
          }

        </ul>
      </div>
      <PopUp />
      <DetailPopUp />
      <FixedButtonArea />
    </div>

  );
}

export default Home;
