import React, { useEffect } from "react";
import SubHeader from "../components/common/SubHeader";
import WhiteWrapper from "../components/partials/addressbook/WhiteWrapper";
import useTranslation from "../services/useTranslation";
import networkService from "../services/networkService";
import urlService from "../services/urlService";
export default function AddressBook(props) {
  let addresses = [];

  const initialize = async () => {
    const response = await networkService.get(urlService.getAddressIndex);
    if (response.IsValid) {
      addresses = response.Payload;
      console.log(addresses);
    }
    console.log(response);
  }
  
  useEffect(() => {
    initialize();
  });
  // console.log(props.addresses);
  const { address_book } = useTranslation();
  return (
    <div>
      <SubHeader title={address_book} />
      <WhiteWrapper data={addresses} />
    </div>
  );
}

