import SubHeader from "../components/common/SubHeader";
import SubTitles from "../components/common/SubTitles";
import OrderDescription from "../components/partials/orderstatus/OrderDescription";
import useTranslation from "../services/useTranslation";
import { Formik } from 'formik';
import networkService from "../services/networkService";
import urlService from "../services/urlService";
import { useState } from "react";
import { useEffect } from "react";

export default function OrderStatus() {
  const [order, setStatus] = useState('');
  const { order_status, order_look_up, search_code, Confirmed,  Processing, On_The_Way, Complete, Cancelled, Reason } = new useTranslation();
  const onSubmitApi = async (values) => {
    const response = await networkService.post(urlService.postTrackOrder, values);
    if (response.IsValid) {
      setStatus(response.Payload);
      let res = response.Payload;
    }
  }

  useEffect(() => {

  }, [order]);

  const renderStatus = () => {
    // console.log(status);
    let status = order.status;

    if (status == 'Confirmed') {
      let st = status;
      return (
        <div >
          <div className="col-lg-6 ml-auto mr-auto p-3">
            <table className="table">
              <thead>
                <tr>
                  <th className="border-0 " style={{ textAlign: "center" }}>
                    <h2>
                      <u>{order_status}</u>
                    </h2>
                    <ul className="order_status_view">
                      <li className="active">
                        <strong className="text-success">{Confirmed}</strong>
                      </li>
                      <li>
                        <span>
                          <strong>{Processing}</strong>
                        </span>
                      </li>
                      <li>
                        <span>
                          <strong>{On_The_Way}</strong>
                        </span>
                      </li>
                      <li>
                        <span>
                          <strong>{Complete}</strong>
                        </span>
                      </li>
                    </ul>
                  </th>
                </tr>
              </thead>
            </table>
          </div>;

      </div>
      );
    } else if (status == 'Processing') {
      let st = status;
      return (
        <div >
          <div className="col-lg-6 ml-auto mr-auto p-3">
            <table className="table">
              <thead>
                <tr>
                  <th className="border-0 " style={{ textAlign: "center" }}>
                    <h2>
                      <u>{order_status}</u>
                    </h2>
                    <ul className="order_status_view">
                      <li >
                        <strong>{Confirmed}</strong>
                      </li>
                      <li className="active">
                        <span>
                          <strong className="text-success">{Processing}</strong>
                        </span>
                      </li>
                      <li >
                        <span>
                          <strong>{On_The_Way}</strong>
                        </span>
                      </li>
                      <li>
                        <span>
                          <strong>{Complete}</strong>
                        </span>
                      </li>
                    </ul>
                  </th>
                </tr>
              </thead>
            </table>
          </div>;

      </div>
      );
    } else if (status == 'On The Way') {
      let st = status;
      return (
        <div >
          <div className="col-lg-6 ml-auto mr-auto p-3">
            <table className="table">
              <thead>
                <tr>
                  <th className="border-0 " style={{ textAlign: "center" }}>
                    <h2>
                      <u>{order_status}</u>
                    </h2>
                    <ul className="order_status_view">
                      <li >
                        <strong>{Confirmed}</strong>
                      </li>
                      <li >
                        <span>
                          <strong >{Processing}</strong>
                        </span>
                      </li>
                      <li className="active" >
                        <span>
                          <strong className="text-success">{On_The_Way}</strong>
                        </span>
                      </li>
                      <li>
                        <span>
                          <strong>{Complete}</strong>
                        </span>
                      </li>
                    </ul>
                  </th>
                </tr>
              </thead>
            </table>
          </div>;

      </div>
      );
    } else if (status == 'Complete') {
      let st = status;
      return (
        <div >
          <div className="col-lg-6 ml-auto mr-auto p-3">
            <table className="table">
              <thead>
                <tr>
                  <th className="border-0 " style={{ textAlign: "center" }}>
                    <h2>
                      <u>{order_status}</u>
                    </h2>
                    <ul className="order_status_view">
                      <li >
                        <strong>{Confirmed}</strong>
                      </li>
                      <li >
                        <span>
                          <strong >{Processing}</strong>
                        </span>
                      </li>
                      <li >
                        <span>
                          <strong>{On_The_Way}</strong>
                        </span>
                      </li>
                      <li className="active">
                        <span>
                          <strong className="text-success">{Complete}</strong>
                        </span>
                      </li>
                    </ul>
                  </th>
                </tr>
              </thead>
            </table>
          </div>;

      </div>
      );
    } else {
      if (status == 'Cancelled') {
        let st = status;
        return (
          <div className="col-lg-6 ml-auto mr-auto p-3">
            <table className="table">
              <thead>
                <tr>
                  <th className="border-0 " style={{ textAlign: "center" }}>
                    <h2>
                      <u>{order_status}</u>
                    </h2>
                    <ul className="order_status_view">
                      <li className="active">
                        <span>
                          <strong className="text-danger">{Cancelled}</strong>
                        </span>
                      </li>
                    </ul>
                    <p>
                      {order.cancel_notes != null ? <b>{Reason}:</b> : ''}{order.cancel_notes}
                    </p>
                  </th>
                </tr>
              </thead>
            </table>
          </div>

        );
      }
    }
  }
  return (
    <div>
      <SubHeader title={order_status} />
      <div className="white_wrapper cart">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <SubTitles title={order_look_up} />
              <OrderDescription />
              <hr />
              <div>
                {renderStatus()}
                <Formik
                  initialValues={{ order_no: '' }}
                  validate={values => {
                    const errors = {};
                    if (!values.order_no) {
                      errors.order_no = 'Order # is required';
                    }
                    return errors;
                  }}
                  onSubmit={(values, { setSubmitting, resetForm }) => {
                    setTimeout(() => {

                      onSubmitApi(values)

                      const data = {
                        order_no: values.order_no,
                      }
                      setSubmitting(false);
                    }, 400);
                  }

                  }
                >
                  {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    /* and other goodies */
                  }) => (
                      <form className="contactus" onSubmit={handleSubmit}>
                        <h6 className="mt-4">{search_code}</h6>
                        <div className="form-group">
                          <input
                            maxLength={100}
                            type="text"
                            className="form-control mt-4"
                            placeholder='Search...'
                            name="order_no"
                            onChange={handleChange}
                            value={values.order_no}
                          />
                          <p className="text-danger">{errors.order_no && touched.order_no && errors.order_no}</p>
                        </div>
                        <button className="primary mt-3 mb-3" type="submit" disabled={isSubmitting}>
                          {order_status}
                        </button>
                      </form>
                    )}
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}