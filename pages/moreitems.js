import SubHeader from "../components/common/SubHeader";
import WhiteWrapper from "../components/partials/moreitems/WhiteWrapper";
import { useRouter } from "next/router";

export default function MoreItems() {
    const router = useRouter();
    const { data, categoryName } = router.query;
    const products = JSON.parse(data);
    console.log(products);
    return (
        <div>
            <SubHeader title={categoryName} />
            <WhiteWrapper products={products} />
        </div>
    );
}