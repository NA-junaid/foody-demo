import React from "react";
import Header from "../components/common/Header";
import Slider from "../components/partials/Home/Slider";
import VendorDetail from "../components/partials/Home/VendorDetail";
import SearchFields from "../components/partials/Home/SearchFields";
import PopUp from "../components/partials/Home/PopUp";
import DetailPopUp from "../components/partials/Home/DetailPopUp";
import FixedButtonArea from "../components/FixedButtonArea";
import CategorySection from "../components/partials/index3/CategorySection";
import useSetup from "../services/useSetup";
import { useState, useEffect } from "react";

function Home({products}) {
return (
        <div>

            <Header />
            <Slider />
            <VendorDetail />
            <hr />
            <SearchFields />
            {/* Products Categories */}
            <div className="products_area">
                <div id="accordion" className="accordion  mt-4 mb-4">
                    <div className="card  mb-0">

                        {
                            [1, 2, 3].map(product => {
                                return (
                                    <CategorySection products={products}/>
                                )
                            })
                        }
              
                    </div>
                </div>
            </div>

            <PopUp />
            <DetailPopUp />
            <FixedButtonArea />
        </div>

    );
}

export default Home;
