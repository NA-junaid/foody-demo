import CheckoutHeader from "../components/partials/checkout/CheckoutHeader";
import StepWizard from "../components/partials/checkout/StepWizard";
import CheckoutForm from "../components/partials/checkout/CheckoutForm";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { setStep } from "../redux/actions/checkoutStepAction";

export default function CheckOut() {
  const webSetting = useSelector((state) => state.webSetting);
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setStep(1));
  });

  return (
    <div>
      <CheckoutHeader />
      <div className="checkout">
        <StepWizard />
        <CheckoutForm />
      </div>
    </div>
  );
}
