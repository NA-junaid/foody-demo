import {useState, useEffect} from 'react';
import axios from "axios";

function useAxios (initialUrl,initialParams={}) {
    
    const [url, setUrl] = useState(initialUrl)
    const [type, setType] = useState('get')
    const [params, setParams] = useState(initialParams)

    const [response, setResponse] = useState(null)
    const [loading, setLoading] = useState(false)
    const [hasError, setHasError] = useState(false)

    useEffect(() => {
        setLoading(true)

        const api = "https://services.nextaxe.com/api/";
        //const api = 'http://localhost:3000/api/';

        const uri = `${api}${url}?store_id=210`;

        let options = {
            method: type,
            url: uri,
            data: null,
        };

        if (process.browser) {
            if (localStorage != undefined) {
              const token = localStorage.getItem("token");
      
              if (token != null) {
                options.headers = {
                  Accept: "application/json",
                  Authorization: `Bearer ${token}`,
                  //"X-Requested-With": "XMLHttpRequest",
                };
              }
            }
          }

          if (type == "GET") {
            if (data != undefined || data != null) {
              options.params = data;
            }
          } else if (type == "POST") {
            if (data != undefined || data != null) {
              options.data = qs.stringify(data);
            }
          }

          axios(options)
          .then((response) => {
            setResponse(response.data);
            setLoading(false)
          })
          .catch((error) => {
            setHasError(true)
            setLoading(false)
          });

    }, [ url ])

    return [ response, loading, hasError,setUrl , setParams,setType ]
}

export default useAxios;